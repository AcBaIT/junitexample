package esslingen;

import java.sql.Connection;
import java.util.Scanner;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MyJdbc {
	private static Connection connection;
	private Statement statement;
	private static ResultSet resultSet;
	private static PreparedStatement ps;

	String url = "jdbc:mysql://localhost:3306/testdb";
	String user = "ab";
	String password = "test";
	String dbName = "testdb";

	// If we want to create another Table with just a little function, use this
	public void createTable() {
		try {
			connection = createConnectionToDatabase();

			PreparedStatement create = connection
					.prepareStatement("CREATE TABLE IF NOT EXISTS mytestdb(id int NOT NULL AUTO_INCREMENT, "
							+ "firstname varchar(255), lastname varchar(255), PRIMARY KEY(id))");
			create.executeUpdate();
			System.out.println("Created");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// Connecting to our database
	public Connection createConnectionToDatabase() {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			System.out.println("Successfully connected: " + con);
			return con;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void insertDataIntoTable(String firstName, String secondName) throws SQLException {
		Connection innerConnection = createConnectionToDatabase();
		ps = innerConnection.prepareStatement("insert into testdb.myTestDB values (default,?,?)");
		ps.setString(1, firstName);
		ps.setString(2, secondName);
		ps.executeUpdate();
		closeAllConnectionsToDatabase();
	}

	public void deleteDataFromTable(String firstName, String secondName) throws SQLException {
		Connection innerConnection = createConnectionToDatabase();
		ps = innerConnection
				.prepareStatement("DELETE FROM myTestDB WHERE myTestDB.firstname = ? AND myTestDB.lastname = ?");
		ps.setString(1, firstName);
		ps.setString(2, secondName);
		ps.executeUpdate();
		closeAllConnectionsToDatabase();
	}

	public String printTableToTextArea() throws SQLException {
		Connection innerConnection = createConnectionToDatabase();
		ResultSet res = resultSet;
		String entriesFromDB = "";
		statement = innerConnection.createStatement();
		res = statement.executeQuery("SELECT * FROM myTestDB");
		entriesFromDB += ("(ID): \t First Name,  \t\tLast Name\n");

		while (res.next()) {
			int id = res.getInt("id");
			String userFirstname = res.getString("firstname");
			String userLastname = res.getNString("lastname");

			entriesFromDB += "(" + id + "): \t\t " + userFirstname + ",  \t\t" + userLastname + "\n";
		}
		closeAllConnectionsToDatabase();
		return entriesFromDB;
	}

	private void closeAllConnectionsToDatabase() throws SQLException {

		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("\nResultSet already null");
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("connection already null");
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("statement already null");
		}
		System.out.println("\n\nDisconnected\n");
	}

	// Should be working, even though this database seems to not be
	// rooted/incorporated in MySQL -> empty file is exported
	/*
	 * public void export() throws Exception { Process exec =
	 * Runtime.getRuntime().exec(new String[]{"cmd.exe","/c",
	 * "C:\\xampp\\mysql\\bin\\mysqldump.exe C:\\xampp\\mysql\\data\\mytestdb -u "
	 * +user+" -p"+password+ " > C:\\DBSafe\\"+dbName+".sql;"});
	 * if(exec.waitFor()==0) { //normally terminated, a way to read the output
	 * System.out.println("\n"); InputStream inputStream = exec.getInputStream();
	 * byte[] buffer = new byte[inputStream.available()]; inputStream.read(buffer);
	 * 
	 * String str = new String(buffer); System.out.println(str); } else {
	 * //abnormally terminated, list error/output System.out.println("\n");
	 * InputStream errorStream = exec.getErrorStream(); byte[] buffer = new
	 * byte[errorStream.available()]; errorStream.read(buffer);
	 * 
	 * String str = new String(buffer); System.out.println(str); } }
	 */
}
