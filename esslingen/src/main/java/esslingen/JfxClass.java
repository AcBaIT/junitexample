package esslingen;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class JfxClass extends Application {

		private static Scene scene;
	    @Override
	    public void start(Stage stage) throws IOException {
	        URL url = JfxClass.class.getResource("/esslingen.fxml");
	        FXMLLoader fxmlLoader = new FXMLLoader(url);
	        Parent p = fxmlLoader.load();
	        
	        scene = new Scene(p, 640, 480);
	        stage.setScene(scene);
	        stage.setTitle("MyOwnFXML");
	        stage.show();
	    }
	    
	    static void setRoot(String fxml) throws Exception {
	    	scene.setRoot(loadFXML(fxml));
	    }
	    
	    private static Parent loadFXML(String fxml) throws IOException {
	        FXMLLoader fxmlLoader = new FXMLLoader(JfxClass.class.getResource(fxml + ".fxml"));
	        return fxmlLoader.load();
	    }
	    
	    public static void main(String[] args) {
	        launch();
	    }
	}
