package esslingen;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class JfxMethodHandler {

	MyJdbc jdbc = new MyJdbc();	
	@FXML private TextField firstName;
	@FXML private TextField secondName;
	@FXML private TextArea textArea;
		
	/*Restrict user input to TextArea with 
	 * TextFormatter and ExpectedTextFilter*/
	
	@FXML
	private void entryButton() throws SQLException {
		jdbc.insertDataIntoTable(firstName.getText(), secondName.getText());
		textArea.setText(jdbc.printTableToTextArea());
	}

	@FXML
	private void deleteButton() throws SQLException {
		jdbc.deleteDataFromTable(firstName.getText(), secondName.getText());
		textArea.setText(jdbc.printTableToTextArea());
	}

	@FXML
	private void printButton() throws SQLException {
		jdbc.printTableToTextArea();
		textArea.setText(jdbc.printTableToTextArea());
	}
	
	@FXML
	private void exitButton() {
		System.exit(-1);
	}

}
